"use strict";

var _ = require('lodash')
  , HTTPError = require('yessql-core/errors/HTTPError')
  , AccessError = require('yessql-core/errors/AccessError')
  , Promise = require('bluebird')
  , SqlModel = require('yessql-core/models/SqlModel')
  , NotFoundError = require('yessql-core/errors/NotFoundError');

// Token used to indicate that no result was returned from a handler.
var NO_RESULT = {};

/**
 * @constructor
 */
function Route(opt) {
  /**
   * @type {express.Router}
   */
  this.expressRouter = opt.expressRouter;
  /**
   * @type {String}
   */
  this.method = opt.method;
  /**
   * @type {String|RegExp}
   */
  this.path = opt.path;
  /**
   * @type {function(req, transaction):boolean}
   */
  this.defaultAuthHandler = opt.defaultAuthHandler;
  /**
   * @type {Number}
   */
  this.unauthenticatedStatusCode = opt.unauthenticatedStatusCode;
  /**
   * @type {Number}
   */
  this.expressMiddlewareTimeout = opt.expressMiddlewareTimeout;
  /**
   * @type {Number}
   */
  this.authHandlerTimeout = opt.authHandlerTimeout;
  /**
   * @type {Number}
   */
  this.handlerTimeoutValue = opt.handlerTimeout;
  /**
   * @type {Array.<function (Request, Response, Transaction)>}
   */
  this.authHandlers = [];
  /**
   * @type {Array.<function (Request, Response, function)>}
   */
  this.expressMiddleware = [];
  /**
   * @type {function (Request, Response, Transaction)}
   */
  this.handlerFunc = null;
  /**
   * @type {Array.<function(Error, IncomingMessage)>}
   */
  this.retryOnErrorMatchers = [];
  /**
   * @type {Number}
   */
  this.retryCountLimit = opt.retryCountLimit;
}

/**
 * Installs an express middleware to the route.
 *
 * If the route handler is transactional (has `transaction` as a third parameter),
 * the same transaction is exposed to the middleware as `req.$yessqlTransaction`.
 *
 * @param {function(IncomingMessage, Transaction, function)} middleware
 * @returns {Route}
 */
Route.prototype.middleware = function (middleware) {
  if (this.handlerFunc) {
    throw new Error('You must call middleware(func) before handler(func)');
  }

  this.expressMiddleware.push(middleware);
  return this;
};

/**
 * Installs an authentication handler for the route.
 *
 * A global timeout can be set for all auth handler functions by setting the `authHandlerTimeout` option
 * to the desired timeout value in milliseconds. If the handler function does not return a value,
 * or has returned a promise which is not finished within this period, an HTTP error with the
 * status code 503 is returned.
 *
 * @see Router#get for examples.
 * @param {function(IncomingMessage, Transaction)=} authHandler
 * @returns {Route}
 */
Route.prototype.auth = function (authHandler) {
  if (this.handlerFunc) {
    throw new Error('You must call auth(func) before handler(func)');
  }

  this.authHandlers.push(authHandler || _.constant(true));
  return this;
};

/**
 * Makes the route public by removed all authentication (including the defaultAuth method).
 *
 * @returns {Route}
 */
Route.prototype.public = function () {
  this.authHandlers = [];
  this.defaultAuthHandler = null;
  return this;
};

/**
 * Installs a handler for the route.
 *
 * A global timeout can be set for all handler functions by setting the `handlerTimeout` option
 * to the desired timeout value in milliseconds. If the handler function does not return a value,
 * or has returned a promise which is not finished within this period, an HTTP error with the
 * status code 503 is returned.
 *
 * The global timeout can be altered (or even disabled) on a per-route level by calling the
 * handlerTimeout function.
 *
 * @see Router#get for examples.
 * @param {function(IncomingMessage, ServerResponse, Transaction)} handler
 * @returns {Route}
 */
Route.prototype.handler = function (handler) {
  if (this.handlerFunc) {
    throw new Error('handler(func) can be called just once per Route instance');
  }

  this.handlerFunc = handler;
  this.execute_();
  return this;
};

/**
 * Overrides (or disables) the global handler timeout, if any, for this route.
 *
 * @param {Number} timeout The timeout, in milliseconds, or null to disable.
 * @returns {Route}
 */
Route.prototype.handlerTimeout = function (timeout) {
  this.handlerTimeoutValue = timeout;
  return this;
};

/**
 * Makes the route request handling repeat if a matching error has been encountered.
 *
 * The parameter is a single function which can inspect the error and the request objects,
 * and should return a (promise resolving to a) truthy value if the handling should be retried.
 *
 * A promise can be used to e.g. delay the retry attempt after a back-off time period.
 *
 * The handling is repeated in its entirety, in a fresh transaction if applicable,
 * starting from running any Express middleware and authentication handlers, and then running
 * the route handler function.
 *
 * The maximum number of retries (not counting the initial handling attempt) can be limited
 * using the `retryCountLimit` option. Setting it to 0 disables retrying globally. The default is 5.
 *
 * Multiple matchers can be added by chaining more .retryOnError calls to the route definition. The
 * handling is repeated if one or more of the configured matchers matches the thrown error.
 *
 * @param {function(Error, IncomingMessage)} matcher
 */
Route.prototype.retryOnError = function (matcher) {
  this.retryOnErrorMatchers.push(matcher);
  return this;
};

/**
 * @private
 */
Route.prototype.execute_ = function () {
  var self = this;

  this.expressRouter[this.method](this.path, function (req, res, next) {
    self.handlerMiddleware_(req, res, next);
  });
};

/**
 * @private
 */
Route.prototype.handlerMiddleware_ = function (req, res, next) {
  var self = this;
  // If the handler takes three arguments we assume that the third one is a transaction.
  // In this case we start a transaction and pass it to the handler.
  var transactionNeeded = this.handlerFunc.length === 3;
  var promise = Promise.resolve();

  if (transactionNeeded) {
    promise = SqlModel.transaction(function (trx) {
      return self.handle_(req, res, trx);
    }, req.db);
  } else {
    promise = promise.then(function () {
      return self.handle_(req, res, null);
    });
  }

  return promise.then(function (result) {
    if (result === NO_RESULT) {
      return;
    }
    if (!result && !_.isString(result)) {
      throw new NotFoundError();
    } else {
      sendResult(result, req, res);
    }
  }).catch(function maybeRetriableError(err) {
    if (!_.isEmpty(self.retryOnErrorMatchers)) {
      return Promise.all(_.map(self.retryOnErrorMatchers,
        function (matcher) {
          return matcher(err, req);
        }))
        .then(function (matches) {
          if (_.any(matches)) {
            // Let's see if we have retries left
            if (_.isNumber(req.$yessqlRetryCount)) {
              req.$yessqlRetryCount++;
            } else {
              req.$yessqlRetryCount = 0;
            }

            if (req.$yessqlRetryCount < self.retryCountLimit) {
              // Yes we have and error warrants retrying, chain new attempt off this one
              return self.handlerMiddleware_(req, res, next);
            }
          }
          // Re-throw error, it was not one we want to retry on
          throw err;
        })
    } else {
      // Propagate error, we don't retry on anything
      throw err;
    }
  }).catch(next);
};

/**
 * @private
 */
Route.prototype.handle_ = function (req, res, transaction) {
  var self = this;
  var context = {};
  var authHandlers = this.authHandlers;
  var promise = Promise.resolve();

  // Expose transaction, if any, to express middleware
  if (transaction) {
    req.$yessqlTransaction = transaction;
  }

  _.forEach(self.expressMiddleware, function (middleware) {
    promise = promise.then(function () {
      return executeMiddleware(req, res, middleware);
    });

    if (_.isNumber(self.expressMiddlewareTimeout)) {
      promise = promise
        .timeout(self.expressMiddlewareTimeout)
        .catch(potentialTimeoutHandler);
    }
  });

  // Remove transaction from request - we want everybody to use it as a parameter in auth and route handlers
  // (it being in their parameters is the only reason we ever create a transaction)
  if (transaction) {
    promise = promise.then(function clearMiddlewareTransaction() {
      delete req.$yessqlTransaction;
    });
  }

  if (_.isEmpty(authHandlers) && this.defaultAuthHandler) {
    authHandlers = [this.defaultAuthHandler];
  }

  // If authentication handlers have been registered, first check
  // that a user has logged in.
  if (!_.isEmpty(authHandlers) && !req.user) {
    throw new HTTPError(this.unauthenticatedStatusCode);
  }

  _.forEach(authHandlers, function (authHandler) {
    promise = promise.then(function () {
      return authHandler.call(context, req, transaction);
    }).then(function (ret) {
      if (!ret) {
        throw new AccessError();
      }
    });

    if (_.isNumber(self.authHandlerTimeout)) {
      promise = promise
        .timeout(self.authHandlerTimeout)
        .catch(potentialTimeoutHandler);
    }
  });

  return promise.then(function () {
    var result = self.handlerFunc.call(context, req, res, transaction);

    // If there is no return value (or the return value is undefined) assume that
    // the handler calls res.end(), res.send() or similar method explicitly.
    if (_.isUndefined(result)) {
      return NO_RESULT;
    } else {
      if (_.isNumber(self.handlerTimeoutValue)) {
        result = Promise.resolve(result)
          .timeout(self.handlerTimeoutValue)
          .catch(potentialTimeoutHandler);
      }

      return result;
    }
  });
};

module.exports = Route;

/**
 * @private
 */
function executeMiddleware(req, res, middleware) {
  return new Promise(function (resolve, reject) {
    var next = function (err) {
      if (err) {
        reject(err);
      } else {
        resolve();
      }
    };
    middleware(req, res, next);
  });
}

/**
 * @private
 */
function sendResult(result, req, res) {
  if (_.isObject(result)) {
    res.set('Content-Type', 'application/json');
    // Pretty print json in development and testing modes.
    if (req.app.config.profile === 'development' || req.app.config.profile === 'testing') {
      res.send(JSON.stringify(result, null, 2));
    } else {
      res.send(JSON.stringify(result));
    }
  } else {
    res.send(result);
  }
}

/**
 * @private
 */
function potentialTimeoutHandler(error) {
  if (error instanceof Promise.TimeoutError) {
    throw new HTTPError(503, 'handler function timeout hit');
  } else {
    throw error;
  }
}
