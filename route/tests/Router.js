var _ = require('lodash')
  , expect = require('expect.js')
  , Promise = require('bluebird')
  , Router = require('../router/Router')
  , HTTPError = require('yessql-core/errors/HTTPError')
  , AccessError = require('yessql-core/errors/AccessError')
  , NotFoundError = require('yessql-core/errors/NotFoundError');

describe('Router', function () {
  var transaction;
  var transactionEnded;
  var request;
  var response;
  var mockExpressRouter;
  var router;

  beforeEach(function () {
    // Mock knex.js transaction object.
    transaction = {};
    transactionEnded = false;
    // Mock express request object.
    request = {
      user: {},
      db: {
        // This is called from SqlModel to start a transaction.
        transaction: spy(function (cb) {
          return new Promise(function (resolve, reject) {
            transaction.commit = spy(resolve);
            transaction.rollback = spy(reject);
            cb(transaction);
          }).then(function (data) {
            transactionEnded = true;
            return data;
          }).catch(function (err) {
            transactionEnded = true;
            throw err;
          });
        })
      },
      // Mock express application.
      app: {
        config: {}
      }
    };
  });

  beforeEach(function () {
    // Mock express response object.
    response = {
      statusCode: 200,
      send: spy(function (data) {
        this.sentData = data;
        this.end();
        return this;
      }),
      set: spy(function () {
        return this;
      }),
      status: spy(function (statusCode) {
        this.statusCode = statusCode;
        return this;
      }),
      end: spy(function () {
        this.onEnd();
        return this;
      }),
      // This is not a response's method.
      onEnd: function () {
        // Implement this in tests.
      }
    };
  });

  beforeEach(function () {
    // Mock express Router object.
    mockExpressRouter = {
      get: createMockRouterMethod('get'),
      put: createMockRouterMethod('put'),
      post: createMockRouterMethod('post'),
      patch: createMockRouterMethod('patch'),
      delete: createMockRouterMethod('delete'),
      simulateRequest: function (req, res, next) {
        this.handler(req, res, next);
      }
    };
    function createMockRouterMethod(method) {
      return function (path, handler) {
        this.method = method;
        this.path = path;
        this.handler = handler;
      };
    }
  });

  beforeEach(function () {
    router = new Router(mockExpressRouter);
  });

  _.each(['get', 'put', 'post', 'delete', 'patch'], function (method) {

    describe('.' + method + '()', function () {

      it('should call ' + method + '() of the wrapped express router', function () {
        router[method]('/some/path').handler(_.noop);
        expect(mockExpressRouter.method).to.equal(method);
        expect(mockExpressRouter.path).to.equal('/some/path');
      });

      it('should be able to return json from handler', function (done) {
        var sendData = {some: 'data'};
        var transactionSpy = request.db.transaction;
        var endSpy = response.end;
        var sendSpy = response.send;

        response.onEnd = function () {
          expect(this.statusCode).to.equal(200);
          expect(this.sentData).to.eql(JSON.stringify(sendData));
          expect(transactionSpy.calls).to.have.length(0);
          expect(endSpy.calls).to.have.length(1);
          expect(sendSpy.calls).to.have.length(1);
          done();
        };

        router[method]('/some/path').handler(function (req, res) {
          expect(req).to.equal(request);
          expect(res).to.equal(response);
          return sendData;
        });

        mockExpressRouter.simulateRequest(request, response, function (err) {
          done(err);
        });
      });

      it('should be able to return a promise from handler', function (done) {
        var sendData = {some: 'data'};
        var transactionSpy = request.db.transaction;
        var endSpy = response.end;
        var sendSpy = response.send;

        response.onEnd = function () {
          expect(this.statusCode).to.equal(200);
          expect(this.sentData).to.eql(JSON.stringify(sendData));
          expect(transactionSpy.calls).to.have.length(0);
          expect(endSpy.calls).to.have.length(1);
          expect(sendSpy.calls).to.have.length(1);
          done();
        };

        router[method]('/some/path').handler(function (req, res) {
          expect(req).to.equal(request);
          expect(res).to.equal(response);
          return Promise.resolve(sendData);
        });

        mockExpressRouter.simulateRequest(request, response, function (err) {
          done(err);
        });
      });

      it('should be able to return a string from handler', function (done) {
        var sendSpy = response.send;

        response.onEnd = function () {
          expect(this.statusCode).to.equal(200);
          expect(this.sentData).to.eql('this is a string');
          expect(sendSpy.calls).to.have.length(1);
          done();
        };

        router[method]('/some/path')
          .auth(function () {
            return true;
          })
          .handler(function () {
            return 'this is a string';
          });

        mockExpressRouter.simulateRequest(request, response, function (err) {
          done(err);
        });
      });

      it('should be able to return a string promise from handler', function (done) {
        var sendSpy = response.send;

        response.onEnd = function () {
          expect(this.statusCode).to.equal(200);
          expect(this.sentData).to.eql('this is a string');
          expect(sendSpy.calls).to.have.length(1);
          done();
        };

        router[method]('/some/path')
          .auth(function () {
            return true;
          })
          .handler(function () {
            return Promise.resolve('this is a string');
          });

        mockExpressRouter.simulateRequest(request, response, function (err) {
          done(err);
        });
      });

      it('handler should pass errors to the next middleware (sync)', function (done) {
        var error = new Error();
        var endSpy = response.end;
        var sendSpy = response.send;
        var transactionSpy = request.db.transaction;

        router[method]('/some/path').handler(function () {
          throw error;
        });

        mockExpressRouter.simulateRequest(request, response, function (err) {
          expect(err).to.equal(error);
          // Should not have called response.end or response.send or started a transaction.
          expect(endSpy.calls).to.have.length(0);
          expect(sendSpy.calls).to.have.length(0);
          expect(transactionSpy.calls).to.have.length(0);
          done();
        });
      });

      it('handler should pass errors to the next middleware (async)', function (done) {
        var error = new Error();
        var endSpy = response.end;
        var sendSpy = response.send;
        var transactionSpy = request.db.transaction;

        router[method]('/some/path').handler(function () {
          return Promise.reject(error);
        });

        mockExpressRouter.simulateRequest(request, response, function (err) {
          expect(err).to.equal(error);
          // Should not have called response.end or response.send or started a transaction.
          expect(endSpy.calls).to.have.length(0);
          expect(sendSpy.calls).to.have.length(0);
          expect(transactionSpy.calls).to.have.length(0);
          done();
        });
      });

      it('returning null should result in 404 response', function (done) {
        var endSpy = response.end;
        var sendSpy = response.send;

        router[method]('/some/path').handler(function () {
          return null;
        });

        mockExpressRouter.simulateRequest(request, response, function (err) {
          expect(err instanceof HTTPError).to.equal(true);
          expect(err.statusCode).to.equal(404);
          // Should not have called response.end or response.send.
          expect(endSpy.calls).to.have.length(0);
          expect(sendSpy.calls).to.have.length(0);
          done();
        });
      });

      it('should execute handler if all auth handlers return true', function (done) {
        var sendData = {some: 'data'};

        response.onEnd = function () {
          expect(this.statusCode).to.equal(200);
          expect(this.sentData).to.eql(JSON.stringify(sendData));
          done();
        };

        router[method]('/some/path')
          .auth(function (req) {
            expect(req).to.equal(request);
            return true;
          })
          .auth(function (req) {
            expect(req).to.equal(request);
            return true;
          })
          .auth(function (req) {
            expect(req).to.equal(request);
            return true;
          })
          .handler(function (req, res) {
            expect(req).to.equal(request);
            expect(res).to.equal(response);
            return sendData;
          });

        mockExpressRouter.simulateRequest(request, response, function (err) {
          done(err);
        });
      });

      it('should execute handler if auth handler returns a promise that evaluates to true', function (done) {
        var sendData = {some: 'data'};

        response.onEnd = function () {
          expect(this.statusCode).to.equal(200);
          expect(this.sentData).to.eql(JSON.stringify(sendData));
          done();
        };

        router[method]('/some/path')
          .auth(function (req) {
            expect(req).to.equal(request);
            return Promise.resolve(true);
          })
          .handler(function (req, res) {
            expect(req).to.equal(request);
            expect(res).to.equal(response);
            return sendData;
          });

        mockExpressRouter.simulateRequest(request, response, function (err) {
          done(err);
        });
      });

      it('request handler and auth handler should share a request specific `this` context', function (done) {
        response.onEnd = function () {
          done();
        };

        router[method]('/some/path')
          .auth(function () {
            expect(this).to.eql({});
            this.test = 100;
            return true;
          })
          .handler(function () {
            expect(this).to.eql({test: 100});
            return {};
          });

        mockExpressRouter.simulateRequest(request, response, function (err) {
          done(err);
        });
      });

      it('auth handler should throw 401 HTTP error if session user is not found', function (done) {
        var endSpy = response.end;
        var sendSpy = response.send;
        var handlerSpy = spy();

        delete request.user;

        router[method]('/some/path')
          .auth()
          .handler(handlerSpy);

        mockExpressRouter.simulateRequest(request, response, function (err) {
          expect(err instanceof HTTPError).to.equal(true);
          expect(err.statusCode).to.equal(401);
          // Should not have called response.end or response.send or the handler.
          expect(endSpy.calls).to.have.length(0);
          expect(sendSpy.calls).to.have.length(0);
          expect(handlerSpy.calls).to.have.length(0);
          done();
        });
      });

      it('auth handler should throw 202 HTTP error instead of 401 if set via unauthenticatedStatusCode', function (done) {
        delete request.user;

        router = new Router(mockExpressRouter, {
          unauthenticatedStatusCode: 202
        });
        router[method]('/some/path')
          .auth()
          .handler(_.noop);

        mockExpressRouter.simulateRequest(request, response, function (err) {
          expect(err instanceof HTTPError).to.equal(true);
          expect(err.statusCode).to.equal(202);
          done();
        });
      });

      it('should throw 403 HTTP error if auth handler returns false', function (done) {
        var endSpy = response.end;
        var sendSpy = response.send;
        var handlerSpy = spy();

        router[method]('/some/path')
          .auth(function () {
            return false;
          })
          .handler(handlerSpy);

        mockExpressRouter.simulateRequest(request, response, function (err) {
          expect(err instanceof AccessError).to.equal(true);
          expect(err.statusCode).to.equal(403);
          // Should not have called response.end or response.send or the handler.
          expect(endSpy.calls).to.have.length(0);
          expect(sendSpy.calls).to.have.length(0);
          expect(handlerSpy.calls).to.have.length(0);
          done();
        });
      });

      it('should throw 403 HTTP error if any of the auth handlers return a promise that evaluates to false', function (done) {
        var endSpy = response.end;
        var sendSpy = response.send;
        var handlerSpy = spy();

        router[method]('/some/path')
          .auth(function () {
            return true;
          })
          .auth(function () {
            return Promise.resolve(true);
          })
          .auth(function () {
            return Promise.resolve(false);
          })
          .handler(handlerSpy);

        mockExpressRouter.simulateRequest(request, response, function (err) {
          expect(err instanceof AccessError).to.equal(true);
          expect(err.statusCode).to.equal(403);
          // Should not have called response.end or response.send or the handler.
          expect(endSpy.calls).to.have.length(0);
          expect(sendSpy.calls).to.have.length(0);
          expect(handlerSpy.calls).to.have.length(0);
          done();
        });
      });

      it('should throw 503 HTTP error if any of the auth handlers takes longer than the auth handler timeout', function (done) {
        var endSpy = response.end;
        var sendSpy = response.send;
        var handlerSpy = spy();

        router = new Router(mockExpressRouter, {
          authHandlerTimeout: 50
        });

        router[method]('/some/path')
          .auth(function () {
            return true;
          })
          .auth(function () {
            return Promise.delay(10000);
          })
          .auth(function () {
            return Promise.resolve(false);
          })
          .handler(handlerSpy);

        mockExpressRouter.simulateRequest(request, response, function (err) {
          expect(err instanceof HTTPError).to.equal(true);
          expect(err.statusCode).to.equal(503);
          // Should not have called response.end or response.send or the handler.
          expect(endSpy.calls).to.have.length(0);
          expect(sendSpy.calls).to.have.length(0);
          expect(handlerSpy.calls).to.have.length(0);
          done();
        });
      });

      it('should throw 403 HTTP error if any of the auth handlers return a promise that evaluates to false, even if a sufficiently long auth handler timeout is set', function (done) {
        var endSpy = response.end;
        var sendSpy = response.send;
        var handlerSpy = spy();

        router = new Router(mockExpressRouter, {
          authHandlerTimeout: 10000
        });

        router[method]('/some/path')
          .auth(function () {
            return true;
          })
          .auth(function () {
            return Promise.resolve(true);
          })
          .auth(function () {
            return Promise.delay(100).return(false);
          })
          .handler(handlerSpy);

        mockExpressRouter.simulateRequest(request, response, function (err) {
          expect(err instanceof AccessError).to.equal(true);
          expect(err.statusCode).to.equal(403);
          // Should not have called response.end or response.send or the handler.
          expect(endSpy.calls).to.have.length(0);
          expect(sendSpy.calls).to.have.length(0);
          expect(handlerSpy.calls).to.have.length(0);
          done();
        });
      });

      it('should execute handler if auth handler returns a promise that evaluates to true, even when auth handler timeout is enabled', function (done) {
        var sendData = {some: 'data'};

        router = new Router(mockExpressRouter, {
          authHandlerTimeout: 10000
        });

        response.onEnd = function () {
          expect(this.statusCode).to.equal(200);
          expect(this.sentData).to.eql(JSON.stringify(sendData));
          done();
        };

        router[method]('/some/path')
          .auth(function (req) {
            expect(req).to.equal(request);
            return Promise.delay(100).return(true);
          })
          .handler(function (req, res) {
            expect(req).to.equal(request);
            expect(res).to.equal(response);
            return sendData;
          });

        mockExpressRouter.simulateRequest(request, response, function (err) {
          done(err);
        });
      });

      it('should use the defaultAuthHandler if defined', function (done) {
        var endSpy = response.end;
        var sendSpy = response.send;
        var handlerSpy = spy();

        var defaultAuthHandlerSpy = spy(function (req) {
          expect(req).to.equal(request);
          return false;
        });

        router = new Router(mockExpressRouter, {
          defaultAuthHandler: defaultAuthHandlerSpy
        });
        router[method]('/some/path').handler(handlerSpy);

        mockExpressRouter.simulateRequest(request, response, function (err) {
          expect(err instanceof AccessError).to.equal(true);
          expect(err.statusCode).to.equal(403);
          // Should not have called response.end or response.send or the handler.
          expect(endSpy.calls).to.have.length(0);
          expect(sendSpy.calls).to.have.length(0);
          expect(handlerSpy.calls).to.have.length(0);
          expect(defaultAuthHandlerSpy.calls).to.have.length(1);
          done();
        });
      });

      it('should remove all authentication if `.public` is called', function (done) {
        var sendData = {some: 'data'};

        var defaultAuthHandlerSpy = spy(function () {
          return false;
        });

        response.onEnd = function () {
          expect(this.statusCode).to.equal(200);
          expect(this.sentData).to.eql(JSON.stringify(sendData));
          expect(defaultAuthHandlerSpy.calls).to.have.length(0);
          done();
        };

        router = new Router(mockExpressRouter, {
          defaultAuthHandler: defaultAuthHandlerSpy
        });
        router[method]('/some/path')
          .auth(function () {
            return false;
          })
          .public()
          .handler(function () {
            return sendData;
          });

        mockExpressRouter.simulateRequest(request, response, function (err) {
          done(err);
        });
      });

      it('should start a transaction if the handler takes three arguments', function (done) {
        var sendData = {some: 'data'};
        var transactionSpy = request.db.transaction;

        response.onEnd = function () {
          expect(this.statusCode).to.equal(200);
          expect(this.sentData).to.eql(JSON.stringify(sendData));
          expect(transactionSpy.calls).to.have.length(1);
          done();
        };

        router[method]('/some/path')
          .auth(function (req, trx) {
            expect(req).to.equal(request);
            expect(trx).to.equal(transaction);
            return true;
          })
          .handler(function (req, res, trx) {
            expect(req).to.equal(request);
            expect(res).to.equal(response);
            expect(trx).to.equal(transaction);
            return sendData;
          });

        mockExpressRouter.simulateRequest(request, response, function (err) {
          done(err);
        });
      });

      it('transaction.commit should be called and the transaction should end before res.end is called', function (done) {
        var sendData = {some: 'data'};
        var transactionSpy = request.db.transaction;

        response.onEnd = function () {
          expect(this.statusCode).to.equal(200);
          expect(this.sentData).to.eql(JSON.stringify(sendData));
          expect(transactionSpy.calls).to.have.length(1);
          expect(transaction.commit.calls).to.have.length(1);
          expect(transaction.rollback.calls).to.have.length(0);
          expect(transactionEnded).to.equal(true);
          done();
        };

        router[method]('/some/path')
          .auth(function (req, trx) {
            expect(req).to.equal(request);
            expect(trx).to.equal(transaction);
            expect(transactionEnded).to.equal(false);
            return true;
          })
          .handler(function (req, res, trx) {
            expect(req).to.equal(request);
            expect(res).to.equal(response);
            expect(trx).to.equal(transaction);
            expect(transactionEnded).to.equal(false);
            return Promise.delay(100).then(function () {
              expect(transaction.commit.calls).to.have.length(0);
              expect(transaction.rollback.calls).to.have.length(0);
              expect(transactionEnded).to.equal(false);
              return sendData;
            });
          });

        mockExpressRouter.simulateRequest(request, response, function (err) {
          done(err);
        });
      });

      it('should respond with 200 and commit the transaction if request handler finishes within the timeout when one is configured', function (done) {
        var sendData = {some: 'data'};
        var transactionSpy = request.db.transaction;

        router = new Router(mockExpressRouter, {
          handlerTimeout: 10000
        });

        response.onEnd = function () {
          expect(this.statusCode).to.equal(200);
          expect(this.sentData).to.eql(JSON.stringify(sendData));
          expect(transactionSpy.calls).to.have.length(1);
          expect(transaction.commit.calls).to.have.length(1);
          expect(transaction.rollback.calls).to.have.length(0);
          expect(transactionEnded).to.equal(true);
          done();
        };

        router[method]('/some/path')
          .handler(function (req, res, trx) {
            expect(req).to.equal(request);
            expect(res).to.equal(response);
            expect(trx).to.equal(transaction);
            expect(transactionEnded).to.equal(false);
            return Promise.delay(100).then(function () {
              expect(transaction.commit.calls).to.have.length(0);
              expect(transaction.rollback.calls).to.have.length(0);
              expect(transactionEnded).to.equal(false);
              return sendData;
            });
          });

        mockExpressRouter.simulateRequest(request, response, function (err) {
          done(err);
        });
      });

      it('should respond with 200 and commit the transaction if request handler finishes within the timeout when one is configured on a per-route level, although the global timeout is shorter', function (done) {
        var sendData = {some: 'data'};
        var transactionSpy = request.db.transaction;

        router = new Router(mockExpressRouter, {
          handlerTimeout: 50
        });

        response.onEnd = function () {
          expect(this.statusCode).to.equal(200);
          expect(this.sentData).to.eql(JSON.stringify(sendData));
          expect(transactionSpy.calls).to.have.length(1);
          expect(transaction.commit.calls).to.have.length(1);
          expect(transaction.rollback.calls).to.have.length(0);
          expect(transactionEnded).to.equal(true);
          done();
        };

        router[method]('/some/path')
          .handlerTimeout(10000)
          .handler(function (req, res, trx) {
            expect(req).to.equal(request);
            expect(res).to.equal(response);
            expect(trx).to.equal(transaction);
            expect(transactionEnded).to.equal(false);
            return Promise.delay(100).then(function () {
              expect(transaction.commit.calls).to.have.length(0);
              expect(transaction.rollback.calls).to.have.length(0);
              expect(transactionEnded).to.equal(false);
              return sendData;
            });
          });

        mockExpressRouter.simulateRequest(request, response, function (err) {
          done(err);
        });
      });

      it('should respond with 503 and rollback the transaction if request handler does not finish within the timeout period', function (done) {
        var sendData = {some: 'data'};
        var transactionSpy = request.db.transaction;

        router = new Router(mockExpressRouter, {
          handlerTimeout: 50
        });

        response.onEnd = function () {
          done(new Error('expected the request handler not to finish in such a short timeout'));
        };

        router[method]('/some/path')
          .handler(function (req, res, trx) {
            expect(req).to.equal(request);
            expect(res).to.equal(response);
            expect(trx).to.equal(transaction);
            expect(transactionEnded).to.equal(false);
            return Promise.delay(10000).then(function () {
              return sendData;
            });
          });

        mockExpressRouter.simulateRequest(request, response, function (err) {
          expect(err instanceof HTTPError).to.equal(true);
          expect(err.statusCode).to.equal(503);
          // Should have rolled back the transaction
          expect(transactionSpy.calls).to.have.length(1);
          expect(transaction.commit.calls).to.have.length(0);
          expect(transaction.rollback.calls).to.have.length(1);
          expect(transactionEnded).to.equal(true);
          done();
        });
      });

      it('should respond with 503 and rollback the transaction if request handler does not finish within the timeout period configured on a per-route level, although there is no global timeout', function (done) {
        var sendData = {some: 'data'};
        var transactionSpy = request.db.transaction;

        router = new Router(mockExpressRouter, {
          handlerTimeout: null
        });

        response.onEnd = function () {
          done(new Error('expected the request handler not to finish in such a short timeout'));
        };

        router[method]('/some/path')
          .handlerTimeout(50)
          .handler(function (req, res, trx) {
            expect(req).to.equal(request);
            expect(res).to.equal(response);
            expect(trx).to.equal(transaction);
            expect(transactionEnded).to.equal(false);
            return Promise.delay(10000).then(function () {
              return sendData;
            });
          });

        mockExpressRouter.simulateRequest(request, response, function (err) {
          expect(err instanceof HTTPError).to.equal(true);
          expect(err.statusCode).to.equal(503);
          // Should have rolled back the transaction
          expect(transactionSpy.calls).to.have.length(1);
          expect(transaction.commit.calls).to.have.length(0);
          expect(transaction.rollback.calls).to.have.length(1);
          expect(transactionEnded).to.equal(true);
          done();
        });
      });

      it('should propagate other errors and rollback the transaction, even if request handler timeout is set', function (done) {
        var sendData = {some: 'data'};
        var transactionSpy = request.db.transaction;

        router = new Router(mockExpressRouter, {
          handlerTimeout: 10000
        });

        response.onEnd = function () {
          done(new Error('expected the request not to end successfully'));
        };

        router[method]('/some/path')
          .handler(function (req, res, trx) {
            expect(req).to.equal(request);
            expect(res).to.equal(response);
            expect(trx).to.equal(transaction);
            expect(transactionEnded).to.equal(false);
            return Promise.delay(100).then(function () {
              throw new NotFoundError('foo was not there');
            });
          });

        mockExpressRouter.simulateRequest(request, response, function (err) {
          expect(err instanceof HTTPError).to.equal(true);
          expect(err.statusCode).to.equal(404);
          // Should have rolled back the transaction
          expect(transactionSpy.calls).to.have.length(1);
          expect(transaction.commit.calls).to.have.length(0);
          expect(transaction.rollback.calls).to.have.length(1);
          expect(transactionEnded).to.equal(true);
          done();
        });
      });

      it('should not need return value if res.send is called in the handler', function (done) {
        var sendData = {some: 'data'};
        var endSpy = response.end;
        var sendSpy = response.send;

        response.onEnd = function () {
          expect(this.statusCode).to.equal(304);
          expect(this.sentData).to.eql(sendData);
          expect(endSpy.calls).to.have.length(1);
          expect(sendSpy.calls).to.have.length(1);
          done();
        };

        router[method]('/some/path')
          .auth(function () {
            return true;
          })
          .handler(function (req, res) {
            res.status(304).send(sendData);
          });

        mockExpressRouter.simulateRequest(request, response, function (err) {
          done(err);
        });
      });

      it('should not need return value if res.end is called in the handler', function (done) {
        var endSpy = response.end;
        var sendSpy = response.send;

        response.onEnd = function () {
          expect(this.statusCode).to.equal(304);
          expect(this.sentData).to.eql(undefined);
          expect(endSpy.calls).to.have.length(1);
          expect(sendSpy.calls).to.have.length(0);
          done();
        };

        router[method]('/some/path')
          .auth(function () {
            return true;
          })
          .handler(function (req, res) {
            setTimeout(function () {
              res.status(304).end();
            }, 10);
          });

        mockExpressRouter.simulateRequest(request, response, function (err) {
          done(err);
        });
      });

      it('should be able to register express middleware', function (done) {
        var sendSpy = response.send;
        var middlewareSpy = spy(function (req, res, next) {
          expect(req).to.equal(request);
          expect(res).to.equal(response);
          setTimeout(next, 20);
        });

        response.onEnd = function () {
          expect(this.statusCode).to.equal(200);
          expect(this.sentData).to.eql(JSON.stringify({some: 'data'}));
          expect(sendSpy.calls).to.have.length(1);
          expect(middlewareSpy.calls).to.have.length(2);
          done();
        };

        router[method]('/some/path')
          .auth(function () {
            return true;
          })
          .middleware(middlewareSpy)
          .middleware(middlewareSpy)
          .handler(function () {
            return {some: 'data'};
          });

        mockExpressRouter.simulateRequest(request, response, function (err) {
          done(err);
        });
      });

      it('should expose DB transaction to express middleware as req.$yessqlTransaction when route handler is transactional', function (done) {
        var middlewareSpy = spy(function (req, res, next) {
          expect(req).to.equal(request);
          expect(res).to.equal(response);
          expect(req.$yessqlTransaction).to.equal(transaction);
          setTimeout(next, 20);
        });

        response.onEnd = function () {
          expect(this.statusCode).to.equal(200);
          expect(this.sentData).to.eql(JSON.stringify({some: 'data'}));
          expect(middlewareSpy.calls).to.have.length(1);
          done();
        };

        router[method]('/some/path')
          .middleware(middlewareSpy)
          .handler(function (req, res, transaction) {
            _.noop(req, res, transaction);
            return {some: 'data'};
          });

        mockExpressRouter.simulateRequest(request, response, function (err) {
          done(err);
        });
      });

      it('should set no req.$yessqlTransaction when route handler is not transactional', function (done) {
        var middlewareSpy = spy(function (req, res, next) {
          expect(req).to.equal(request);
          expect(res).to.equal(response);
          expect(req).to.not.have.property('$yessqlTransaction');
          setTimeout(next, 20);
        });

        response.onEnd = function () {
          expect(this.statusCode).to.equal(200);
          expect(this.sentData).to.eql(JSON.stringify({some: 'data'}));
          expect(middlewareSpy.calls).to.have.length(1);
          done();
        };

        router[method]('/some/path')
          .middleware(middlewareSpy)
          .handler(function (/* no req, res, transaction */) {
            return {some: 'data'};
          });

        mockExpressRouter.simulateRequest(request, response, function (err) {
          done(err);
        });
      });

      it('failing express middleware should call next() with an error', function (done) {
        var error = new Error();

        var middlewareSpy = spy(function (req, res, next) {
          setTimeout(function () {
            next(error);
          }, 20);
        });

        response.onEnd = function () {
          done(new Error('should not get here'));
        };

        router[method]('/some/path')
          .auth(function () {
            return true;
          })
          .middleware(middlewareSpy)
          .middleware(middlewareSpy)
          .handler(function () {
            done(new Error('should not get here'));
          });

        mockExpressRouter.simulateRequest(request, response, function (err) {
          expect(err).to.equal(error);
          expect(middlewareSpy.calls).to.have.length(1);
          done();
        });
      });

      it('should timeout express middleware if it does not finish in configured timeout period', function (done) {
        var sendSpy = response.send;
        var middlewareSpy = spy(function (req, res, next) {
          expect(req).to.equal(request);
          expect(res).to.equal(response);
          setTimeout(next, 10000);
        });

        response.onEnd = function () {
          done(new Error('expected the request to not end successfully'));
        };

        router = new Router(mockExpressRouter, {
          expressMiddlewareTimeout: 50
        });

        router[method]('/some/path')
          .auth(function () {
            return true;
          })
          .middleware(middlewareSpy)
          .middleware(middlewareSpy)
          .handler(function () {
            return {some: 'data'};
          });

        mockExpressRouter.simulateRequest(request, response, function (err) {
          expect(err instanceof HTTPError).to.equal(true);
          expect(err.statusCode).to.equal(503);
          done();
        });
      });

      it('failing express middleware should call next() with an error even if a sufficiently long timeout is set', function (done) {
        var error = new Error();

        var middlewareSpy = spy(function (req, res, next) {
          setTimeout(function () {
            next(error);
          }, 20);
        });

        response.onEnd = function () {
          done(new Error('should not get here'));
        };

        router = new Router(mockExpressRouter, {
          expressMiddlewareTimeout: 10000
        });

        router[method]('/some/path')
          .auth(function () {
            return true;
          })
          .middleware(middlewareSpy)
          .middleware(middlewareSpy)
          .handler(function () {
            done(new Error('should not get here'));
          });

        mockExpressRouter.simulateRequest(request, response, function (err) {
          expect(err).to.equal(error);
          expect(middlewareSpy.calls).to.have.length(1);
          done();
        });
      });

      it('fast express middleware should work successfully even if middleware timeout is set', function (done) {
        var sendSpy = response.send;
        var middlewareSpy = spy(function (req, res, next) {
          expect(req).to.equal(request);
          expect(res).to.equal(response);
          setTimeout(next, 20);
        });

        response.onEnd = function () {
          expect(this.statusCode).to.equal(200);
          expect(this.sentData).to.eql(JSON.stringify({some: 'data'}));
          expect(sendSpy.calls).to.have.length(1);
          expect(middlewareSpy.calls).to.have.length(2);
          done();
        };

        router = new Router(mockExpressRouter, {
          expressMiddlewareTimeout: 10000
        });

        router[method]('/some/path')
          .auth(function () {
            return true;
          })
          .middleware(middlewareSpy)
          .middleware(middlewareSpy)
          .handler(function () {
            return {some: 'data'};
          });

        mockExpressRouter.simulateRequest(request, response, function (err) {
          done(err);
        });
      });

      it('should retry request handling with a new transaction if first try throws an error matching retryOnError', function (done) {
        var transactionSpy = request.db.transaction;

        var firstTransaction = {};
        var secondTransaction = {};
        transaction = firstTransaction;

        var firstHandlerCall = true;
        var expectedError = new Error('expected error');
        var expectedResult = 'ok';

        router[method]('/some/path')
          .handler(function (req, res, trx) {
            expect(req).to.equal(request);
            expect(res).to.equal(response);
            expect(transactionEnded).to.equal(false);

            if (firstHandlerCall) {
              expect(trx).to.equal(firstTransaction);
              throw expectedError;
            } else {
              expect(trx).to.equal(secondTransaction);
              return expectedResult;
            }
          })
          .retryOnError(function (err, req) {
            expect(err).to.equal(expectedError);
            expect(req).to.equal(request);

            expect(firstHandlerCall).to.equal(true);
            firstHandlerCall = false;

            expect(transactionEnded).to.equal(true);
            transactionEnded = false;
            transaction = secondTransaction;

            return true;
          });

        response.onEnd = function () {
          // Should return successful result to client
          expect(this.statusCode).to.equal(200);
          expect(this.sentData).to.equal(expectedResult);

          // Should have rolled back the first transaction and committed the second
          expect(transactionEnded).to.equal(true);
          expect(transactionSpy.calls).to.have.length(2);
          expect(firstTransaction.rollback.calls).to.have.length(1);
          expect(secondTransaction.commit.calls).to.have.length(1);

          done();
        };

        mockExpressRouter.simulateRequest(request, response, done);
      });

      it('should retry with multiple retryOnError handlers, if at least one matches', function (done) {
        var transactionSpy = request.db.transaction;

        var firstError = new Error('first error');
        var secondError = new Error('second error');
        var expectedResult = 'ok';

        var nthHandlerCall = 0;
        router[method]('/some/path')
          .handler(function (req, res, trx) {
            expect(req).to.equal(request);
            expect(res).to.equal(response);
            expect(trx).to.equal(transaction);

            nthHandlerCall++;
            expect(nthHandlerCall).to.be.within(1, 3);

            switch (nthHandlerCall) {
              case 1:
                throw firstError;
              case 2:
                throw secondError;
              case 3:
                return expectedResult;
            }
          })
          .retryOnError(function retryOnFirst(err, req) {
            expect(req).to.equal(request);
            return err === firstError;
          })
          .retryOnError(function retryOnSecond(err, req) {
            expect(req).to.equal(request);
            return err === secondError;
          });

        response.onEnd = function () {
          // Should return successful result to client
          expect(this.statusCode).to.equal(200);
          expect(this.sentData).to.equal(expectedResult);

          // Should have used three transactions and committed the final transaction
          // (previous ones have been rolled back as per the preceding test)
          expect(transactionEnded).to.equal(true);
          expect(transactionSpy.calls).to.have.length(3);
          expect(transaction.rollback.calls).to.have.length(0);
          expect(transaction.commit.calls).to.have.length(1);

          done();
        };

        mockExpressRouter.simulateRequest(request, response, done);
      });

      it('should not retry request handling if unmatching error is thrown, even if retryOnError used', function (done) {
        var transactionSpy = request.db.transaction;
        var firstHandlerCall = true;

        router[method]('/some/path')
          .handler(function (req, res, trx) {
            expect(req).to.equal(request);
            expect(res).to.equal(response);
            expect(trx).to.equal(transaction);
            expect(transactionEnded).to.equal(false);
            expect(firstHandlerCall).to.equal(true);
            firstHandlerCall = false;
            throw new NotFoundError('nothing here')
          })
          .retryOnError(function (err, req) {
            expect(err).to.be.a(NotFoundError);
            expect(req).to.equal(request);
            return Promise.resolve(false);
          });

        mockExpressRouter.simulateRequest(request, response, function (err) {
          expect(err instanceof HTTPError).to.equal(true);
          expect(err.statusCode).to.equal(404);

          // Should have rolled back the transaction
          expect(transactionEnded).to.equal(true);
          expect(transactionSpy.calls).to.have.length(1);
          expect(transaction.commit.calls).to.have.length(0);
          expect(transaction.rollback.calls).to.have.length(1);

          done();
        });
      });

      it('should fail request handling with latest error if retry count exceeds configured limit', function (done) {
        var transactionSpy = request.db.transaction;
        var handlerCalls = 0;

        router = new Router(mockExpressRouter, {
          retryCountLimit: 2
        });

        var expectedError = new NotFoundError('nope');

        router[method]('/some/path')
          .handler(function alwaysThrow(req, res, trx) {
            expect(req).to.equal(request);
            expect(res).to.equal(response);
            expect(trx).to.equal(transaction);
            expect(transactionEnded).to.equal(false);

            handlerCalls++;
            throw expectedError;
          })
          .retryOnError(function alwaysRetry(err, req) {
            expect(err).to.equal(expectedError);
            expect(req).to.equal(request);

            expect(transactionEnded).to.equal(true);
            transactionEnded = false;

            return true;
          });

        mockExpressRouter.simulateRequest(request, response, function (err) {
          expect(err instanceof HTTPError).to.equal(true);
          expect(err.statusCode).to.equal(404);

          // Should have called handler exactly three times (original try + 2 retries)
          expect(handlerCalls).to.equal(3);

          // Should have rolled back the transaction each time
          expect(transactionSpy.calls).to.have.length(3);
          expect(transaction.commit.calls).to.have.length(0);
          expect(transaction.rollback.calls).to.have.length(1); // only the latest, the spy is reinitialized on each transaction()

          done();
        });
      });
    });
  });

  function spy(func) {
    func = func || _.noop;
    var calls = [];

    var wrapper = function () {
      calls.push(_.map(arguments, function (value) {
        return value;
      }));
      return func.apply(this, arguments);
    };

    wrapper.calls = calls;
    return wrapper;
  }

});
